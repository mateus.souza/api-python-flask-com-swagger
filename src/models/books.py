from unittest.util import _MAX_LENGTH
from flask_restx import fields

from src.server.instance import server

book = server.api.model('Book', {
    'id': fields.String(description='O ID do registro.'),
    'title': fields.String(required=True, min_Length=1, max_Length=200, description='O titulo do livro.')
})